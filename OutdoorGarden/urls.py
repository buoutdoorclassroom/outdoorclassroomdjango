from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static 

from BUOC import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', views.index, name='index'),
    url(r'^about/', views.about, name='about'),
    url(r'^student_information/', views.student_information, name='student_information'),
    url(r'^faculty_information/', views.faculty_information, name='faculty_information'),
    url(r'^event/(?P<pk>\d+)/', views.EventView.as_view(), name='event_view'),
    url(r'^class/(?P<pk>\d+)/', views.ClassroomEventView.as_view(), name='classroom_event_view'),
    url(r'^news/(?P<pk>\d+)/', views.NewsItemView.as_view(), name='news_item_view'),
    url(r'^faq/(?P<pk>\d+)/', views.FaqView.as_view(), name='faq_view'),
    url(r'^document/(?P<pk>\d+)/', views.InfoDocumentView.as_view(), name='info_doc_view'),
    url(r'^picture/(?P<pk>\d+)/', views.PictureUploadView.as_view(), name='picture_upload_view'),
    # url(r'^OutdoorGarden/', include('OutdoorGarden.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

