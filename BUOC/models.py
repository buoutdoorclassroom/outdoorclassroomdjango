from django.db import models
from django import forms


# Create your models here.

class Event(models.Model):
  def __unicode__(self):
    return self.event_name
  def get_absolute_url(self): # Try to use reverse() here later...
    return "/event/%i/" % self.id
  event_name = models.CharField(max_length=128)
  advertisement_date = models.DateTimeField('date advertised')
  scheduled_date = models.DateTimeField('date scheduled')
  description = models.TextField()
  
class ClassroomEvent(models.Model):
  def __unicode__(self):
    return self.course_name
  def get_absolute_url(self): # Try to use reverse() here later...
    return "/class/%i/" % self.id
  advertisement_date = models.DateTimeField('date advertised')
  scheduled_date = models.DateTimeField('date scheduled')
  host_department = models.CharField(max_length=64)
  course_name = models.CharField(max_length=64)
  class_section = models.CharField(max_length=64)
  instructor = models.CharField(max_length=64)
  description = models.TextField() # replace with info document later

class NewsItem(models.Model):
  def __unicode__(self):
    return self.title
  def get_absolute_url(self): # Try to use reverse() here later
    return "/news/%i/" % self.id
  title = models.CharField(max_length=128)
  originally_published = models.DateTimeField('date published')
  last_modified = models.DateTimeField('date modified')
  content = models.TextField()
  
class FrequentlyAskedQuestion(models.Model):
  def __unicode__(self):
    return self.question
  def get_absolute_url(self): # Try to use reverse() later
    return "/faq/%i/" % self.id
  question = models.CharField(max_length=1024)
  answer   = models.TextField()
  originally_published = models.DateTimeField('date published')
  last_modified = models.DateTimeField('date modified')
  
class InfoDocument(models.Model):
  def __unicode__(self):
    return self.title

  def get_absolute_url(self): # Try to use reverse() later
    return "/document/%i/" % self.id

  def INFO_DOCUMENT_PAGES():
    return (
      ('about', 'About Page'),
      ('home', 'Home Page'),
      ('student_info', 'Student Information'),
      ('faculty_info', 'Faculty Information'),
    )
  title = models.CharField(max_length=512)
  body  = models.TextField()
  originally_published = models.DateTimeField('originally published')
  last_modified = models.DateTimeField('last modified')
  original_author = models.CharField(max_length=64)
  last_edit_author = models.CharField(max_length=64)
  appears_on_page = models.CharField(max_length=16, choices=INFO_DOCUMENT_PAGES())

class PictureUpload(models.Model):
  def __unicode__(self):
    return self.title
  def get_absolute_url(self): # Try to use reverse() later
    return "/picture/%i/" % self.id
  title = models.CharField(max_length=1024)
  body = models.TextField()
  originally_published = models.DateTimeField('originally published')
  last_modified = models.DateTimeField('last modified')
  original_photographer = models.CharField(max_length=64)
  last_edit_author = models.CharField(max_length=64)
  appears_on_home_page_carousel = models.BooleanField(default=False)
  picture = models.ImageField(upload_to='picture_uploads/')
  
