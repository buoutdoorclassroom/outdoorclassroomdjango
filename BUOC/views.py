# Create your views here.

from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse 
from django.views import generic
from django.utils import timezone
from django.template import RequestContext, loader
from django.views.generic import DetailView
from random import shuffle

from BUOC.models import Event, ClassroomEvent, NewsItem, FrequentlyAskedQuestion, InfoDocument, PictureUpload

# We use this to generate the sequence used by the carousel..
def getSequence(carousel_list):
  i = 0
  sequenceList = []
  for pic in carousel_list:
    sequenceList.append(i)
    i += 1
  return sequenceList

def getListFromQuerySet(querySetResult):
  resultSeq = []
  for pic in querySetResult:
    resultSeq.append(pic)
  shuffle(resultSeq)
  return resultSeq

# First we will define Model-specific views
class CarouselLoadableView(generic.DetailView):
  def get_context_data(self, **kwargs):
    context = super(CarouselLoadableView, self).get_context_data(**kwargs)
    carousel_pic_list = getListFromQuerySet(PictureUpload.objects.filter(appears_on_home_page_carousel=True))
    context['carousel_pic_list'] = carousel_pic_list
    context['carousel_sequence_list'] = getSequence(carousel_pic_list)
    return context

class EventView(CarouselLoadableView):
  model = Event
  template_name = 'BUOC/event_details.html'

class ClassroomEventView(CarouselLoadableView):
  model = ClassroomEvent
  template_name = 'BUOC/classroom_event_details.html'

class NewsItemView(CarouselLoadableView):
  model = NewsItem
  template_name = 'BUOC/news_item_details.html'

class FaqView(CarouselLoadableView):
  model = FrequentlyAskedQuestion
  template_name = 'BUOC/faq_details.html'

class InfoDocumentView(CarouselLoadableView):
  model = InfoDocument
  template_name = 'BUOC/info_document_details.html'

class PictureUploadView(CarouselLoadableView):
  model = PictureUpload
  template_name = 'BUOC/picture_upload_details.html'

# Now we define Julie's broader views
def index(request):
  latest_news_list = NewsItem.objects.order_by('-originally_published')[:5]
  latest_events_list = Event.objects.order_by('scheduled_date')[:5]
  info_doc_list = InfoDocument.objects.filter(appears_on_page__exact='home')
  carousel_pic_list = getListFromQuerySet(PictureUpload.objects.filter(appears_on_home_page_carousel=True))
  template = loader.get_template('BUOC/home.html')
  context = RequestContext(request, {
    'latest_news_list': latest_news_list,
    'latest_events_list': latest_events_list,
    'info_doc_list': info_doc_list,
    'carousel_pic_list': carousel_pic_list,
    'carousel_sequence_list': getSequence(carousel_pic_list)
  })
  return HttpResponse(template.render(context))

def about(request):
  faq_list = FrequentlyAskedQuestion.objects.all()
  info_doc_list = InfoDocument.objects.filter(appears_on_page__exact='about')
  template = loader.get_template('BUOC/about.html')
  carousel_pic_list = getListFromQuerySet(PictureUpload.objects.filter(appears_on_home_page_carousel=True))
  context = RequestContext(request, {
    'info_doc_list': info_doc_list,
    'faq_list': faq_list,
    'carousel_pic_list': carousel_pic_list,
    'carousel_sequence_list': getSequence(carousel_pic_list)
  })
  return HttpResponse(template.render(context))

def student_information(request):
  info_doc_list = InfoDocument.objects.filter(appears_on_page__exact='student_info')
  full_class_list = ClassroomEvent.objects.order_by('scheduled_date')
  template = loader.get_template('BUOC/student_information.html')
  context = RequestContext(request, {
    'info_doc_list': info_doc_list,
    'full_class_list': full_class_list,
  })
  return HttpResponse(template.render(context))

def faculty_information(request):
  info_doc_list = InfoDocument.objects.filter(appears_on_page__exact='faculty_info')
  template = loader.get_template('BUOC/faculty_information.html')
  context = RequestContext(request, {
    'info_doc_list': info_doc_list
  })
  return HttpResponse(template.render(context))
