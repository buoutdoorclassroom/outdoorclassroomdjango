from django.contrib import admin
from BUOC.models import Event, ClassroomEvent, NewsItem, FrequentlyAskedQuestion, InfoDocument, PictureUpload

class FrequentlyAskedQuestionAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Question and Answer information', {'fields': ['question', 'answer']}),
    ('Date information', {'fields': ('last_modified', 'originally_published') }),
  ]
  list_display = ('question', 'answer')
  search_fields = ['question', 'answer']
  date_hierarchy = 'last_modified'
  list_filter = ['last_modified', 'originally_published']


class InfoDocumentAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Document Data', {'fields': ('title', 'body'), 'classes': ('wide', 'extrapretty'), }),
    ('Calendar Information for this Document', {'fields': ('last_modified', 'originally_published') }),
    ('Presentation Settings', {'fields': ['appears_on_page']}),
  ]
  list_display = ('title', 'body', 'originally_published', 'last_modified')
  search_fields = ['body']
  date_hierarchy = 'last_modified'
  list_filter = ['last_modified', 'originally_published']

class EventAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Event Information', {'fields': ('event_name', 'description'), 'classes': ('wide', 'extrapretty'), }),
    ('Calendar Information', {'fields': ('scheduled_date', 'advertisement_date')}),
  ]
  list_display = ('event_name', 'description', 'scheduled_date', 'advertisement_date')
  search_fields = ['description']
  date_hierarchy = 'scheduled_date'
  list_filter = ['scheduled_date', 'advertisement_date']

class ClassroomEventAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Classroom Event Information', {'fields': ('course_name', 'instructor', 'class_section', 'host_department', 'description'), 'classes': ('wide', 'extrapretty'), }),
    ('Calendar Information', {'fields': ('scheduled_date', 'advertisement_date')}),
  ]
  list_display = ('instructor', 'course_name', 'class_section', 'scheduled_date', 'advertisement_date')
  search_fields = ['description', 'host_department', 'course_name', 'instructor', 'class_section']
  date_hierarchy = 'scheduled_date'
  list_filter = ['scheduled_date', 'advertisement_date']

class NewsItemAdmin(admin.ModelAdmin):
  fieldsets = [
    ('News Announcement', {'fields': ('title', 'content')}),
    ('Calendar Information', {'fields': ('originally_published', 'last_modified')}),
  ]
  list_display = ('title', 'originally_published', 'last_modified', 'content')
  search_fields = ['title', 'content']
  date_hierarchy = 'originally_published'
  list_filter = ['originally_published', 'last_modified']

class PictureUploadAdmin(admin.ModelAdmin):
  fieldsets = [
    ('Picture Information', {'fields': ('title', 'body')}),
    ('Presentation Settings', {'fields': ['appears_on_home_page_carousel']}),
    ('Photograph', {'fields': ['picture']}),
    ('Calendar Information', {'fields': ('originally_published', 'last_modified')}),
  ]
  list_display = ('title', 'originally_published', 'last_modified', 'body')
  search_fields = ['title', 'body']


admin.site.register(Event, EventAdmin)
admin.site.register(ClassroomEvent, ClassroomEventAdmin)
admin.site.register(NewsItem, NewsItemAdmin)
admin.site.register(FrequentlyAskedQuestion, FrequentlyAskedQuestionAdmin)
admin.site.register(InfoDocument, InfoDocumentAdmin)
admin.site.register(PictureUpload, PictureUploadAdmin)
